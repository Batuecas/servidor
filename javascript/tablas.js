//JAVASCRIPT
function tabla2(){
	var xmlhttp=new XMLHttpRequest();
	xmlhttp.onreadystatechange=function(){
		if(this.readyState==4 && this.status==200){
			readXML(this);
		}
	};
	xmlhttp.open("GET", "tabla.xml", true);
	xmlhttp.send();
}

function readXML(xml){
	var x, i , xmlDoc, next_id;
	xmlDoc=xml.responseXML;
	x=xmlDoc.getElementsByTagName("ciudad");
	for(c=0; c<x.length; c++){
		next_id="c"+c+"_nombre";
		document.getElementById(next_id).innerHTML=x[c].getElementsByTagName("nombre")[0].childNodes[0].nodeValue;
		next_id="c"+c+"_temp";
		document.getElementById(next_id).innerHTML=x[c].getElementsByTagName("temp")[0].childNodes[0].nodeValue;
		next_id="c"+c+"_hum";
		document.getElementById(next_id).innerHTML=x[c].getElementsByTagName("hum")[0].childNodes[0].nodeValue;
		next_id="c"+c+"_ruido";
		document.getElementById(next_id).innerHTML=x[c].getElementsByTagName("ruido")[0].childNodes[0].nodeValue;
		next_id="c"+c+"_luz";
		document.getElementById(next_id).innerHTML=x[c].getElementsByTagName("luz")[0].childNodes[0].nodeValue;
		next_id="c"+c+"_ilu";
		document.getElementById(next_id).innerHTML=x[c].getElementsByTagName("ilu")[0].childNodes[0].nodeValue;
	}
}

//JQUERY
$(document).ready(function(){
			$('#tabla_1').show(1, function(){});
			$('#tabla_2').hide(1, function(){});
			$('#tabla_3').hide(1, function(){});
			$('#tabla_4').hide(1, function(){});
			$('#tabla_1').css('margin-top', "10%");
			$('#tabla_2').css('margin-top', "10%");
			$('#tabla_3').css('margin-top', "10%");
			$('#tabla_4').css('margin-top', "10%");
			$('#btn1').css('background-color', "#8888FF");
			$('#btn1').click(function(){
				$('table:visible').hide(1, function(){});
				$('#tabla_1').show(1, function(){});
				$('#btn1').css('background-color', "#8888FF");
				$('#btn2').css('background-color', "");
				var color=$('#btn3').css('background-color');
				if(color!="rgb(255, 0, 0)"){
					$('#btn3').css('background-color', "");
				};
				$('#btn4').css('background-color', "");
			});
			$('#btn2').click(function(){
				$('table:visible').hide(1, function(){});
				$('#tabla_2').show(1, function(){});
				$('#btn1').css('background-color', "");
				$('#btn2').css('background-color', "#8888FF");
				var color=$('#btn3').css('background-color');
				if(color!="rgb(255, 0, 0)"){
					$('#btn3').css('background-color', "");
				};
				$('#btn4').css('background-color', "");
			});
			$('#btn3').click(function(){
				$('table:visible').hide(1, function(){});
				$('#tabla_3').show(1, function(){});
				$('#btn1').css('background-color', "");
				$('#btn2').css('background-color', "");
				$('#btn3').css('background-color', "#8888FF");
				$('#btn4').css('background-color', "");
				$.getJSON("tabla.json", function(tabla){
					var c, next_id;
					for(c=5; c<tabla.ciudad.length+5; c++){
						next_id="#c"+c+"_nombre";
						$(next_id).text(tabla.ciudad[c-5].nombre);
						next_id="#c"+c+"_temp";
						$(next_id).text(tabla.ciudad[c-5].temp);
						next_id="#c"+c+"_hum";
						$(next_id).text(tabla.ciudad[c-5].hum);
						next_id="#c"+c+"_ruido";
						$(next_id).text(tabla.ciudad[c-5].ruido);
						next_id="#c"+c+"_luz";
						$(next_id).text(tabla.ciudad[c-5].luz);
						next_id="#c"+c+"_ilu";
						$(next_id).text(tabla.ciudad[c-5].ilu);	
					}
				})
			});
			$('#btn4').click(function(){
				$('table:visible').hide(1, function(){});
				$('#tabla_4').show(1, function(){});
				$('#btn1').css('background-color', "");
				$('#btn2').css('background-color', "");
				var color=$('#btn3').css('background-color');
				if(color!="rgb(255, 0, 0)"){
					$('#btn3').css('background-color', "");
				};
				$('#btn4').css('background-color', "#8888FF");
			});
		});
