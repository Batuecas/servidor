import sys, socket, select, gzip, threading, time, datetime, os, mysql.connector, ssl, hashlib, functions, subprocess

#ponemos los datos para la base de datos
mydb = mysql.connector.connect(
    host='localhost',
    user='servidor',
    password='1111',
    database='servidor'
)
mycursor=mydb.cursor()
#preparamos las queries para introducir los datos en la base de datos
formulario="INSERT INTO formulario (name, surname, IP, time) VALUES (%s, %s, %s, %s)"
peticion="INSERT INTO peticion (file, language, agent, IP, time, code) VALUES (%s, %s, %s, %s, %s, %s)"
descarga="INSERT INTO descarga (file, url, IP, time) VALUES (%s, %s, %s, %s)"
get_down="SELECT file FROM descarga WHERE IP=%s ORDER BY time DESC"
data="INSERT INTO datos (value, magnitud, sensor, api_key, agent, IP, time) VALUES (%s, %s, %s, %s, %s, %s, %s)"

lista_clientes = []
RECV_BUFFER = 4096 # numero maximo de caracteres que podemos recibir

# funciones basicas del socket HTTP
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) #para evitar el error "Address is already in use"
server_socket.bind(("0.0.0.0", 80))
server_socket.listen(10)
lista_clientes.append(server_socket)

# funciones basicas del socket HTTPS
ssl_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
ssl_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) #para evitar el error "Address is already in use"
ssl_socket.bind(("0.0.0.0", 443))
ssl_socket.listen(10)
lista_clientes.append(ssl_socket)

print("Servidor web empezado en los puertos 80 para HTTP y 443 para HTTPS.\n" )
inactivo=1
tipo=''

while 1:
    readable, writable, exceptional = select.select(lista_clientes, [], [],10)

    #si salta el timeout quiere decir que el servidor lleva 10 segundos sin recibir peticiones
    if not readable and inactivo==1 :
        inactivo=0

    for sock in readable:
        inactivo=1
        #recibimos una nueva conexión HTTPS
        if sock == ssl_socket:
            try:
                sock_cliente, addr = ssl_socket.accept()
                sock_cliente= ssl.wrap_socket(sock_cliente,server_side=True,certfile="key/cert.pem", keyfile="key/key.pem",ssl_version=ssl.PROTOCOL_SSLv23)
                lista_clientes.append(sock_cliente)
            finally:
                print ("[%s:%s] ha hecho una petición\n" % addr)
            
        #recibimos una nueva conexión HTTP
        elif sock == server_socket:
            sock_cliente, addr = server_socket.accept()
            lista_clientes.append(sock_cliente)
            print ("[%s:%s] ha hecho una petición\n" % addr)


	
        #si no hay un cliente nuevo recibimos el mensaje y respondemos 
        else:
            try:
                request = sock.recv(1024).decode('utf-8') 
            except:
                exit

            #si recibimos un mensaje vacío cerramos el socket
            if request=='':
                lista_clientes.remove(sock)
                sock.close()

            else:
                print(str(sock))
                try:
                    ip_cliente=str(sock).split("'")[3]
                except:
                    exit
                today = datetime.datetime.now()
                hora = str(today).split('.')
                print(str(request) + '\n\n\n')

                metodo, archivo, version, idioma, agent, referer = functions.parametros(request)
                if(metodo!='' and archivo!='' and version!='' and idioma!='' and agent!=''):
                    header, response, codigo = functions.ruta(archivo, idioma, version) 
                    exit

                    #si usa el método POST extraemos los datos y los introducimos en la base de datos
                    if metodo == 'POST':
                        if referer.endswith('formulario.html'):
                            header, response, codigo = functions.post_form(request, mycursor, formulario, mydb, header, response, codigo)

                        elif referer.endswith('login.html'):
                            header, response, codigo = functions.post_login(request, mycursor, header, response, codigo)

                        elif referer.endswith('descarga.html'):
                            header, response, codigo=functions.post_down(request, mycursor, descarga, get_down, ip_cliente, hora, mydb, header, response, codigo)

                        elif referer.endswith('datos.html'):
                            header, response, codigo=functions.post_datos(request, agent, ip_cliente, hora, mycursor, data, mydb, header, response, codigo)
                        
                    val=(archivo, idioma, agent, ip_cliente, hora[0], codigo)
                    mycursor.execute(peticion, val)
                    mydb.commit()

                else:
                    header = ''
                    response=''.encode('utf-8')
                
                #juntamos la cabecera y el mensaje, lo enviamos y cerramos el socket de la petición
                final_response = header.encode('utf-8')
                final_response += response
                try:
                    sock.send(final_response)
                except:
                    exit
                if(version!='HTTP/1.1' or archivo.endswith('download.html')):
                    lista_clientes.remove(sock)
                    sock.close()

server_socket.close()
