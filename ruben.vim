" Vim color file
"
" Author: Rubén Comerón
"
" Options with cterm (colored terminal):
"       bold
"       underline
"       undercurl
"       reverse
"       inverse
"       italic
"       standout
"       NONE
"
" Other options:
"       ctermfg (colored terminal foreground)
"       ctermbg (colored terminal background)
"
" Both only accept Black, Blue, Green, Cyan, Red, Magenta, Brown, Gray/Grey, Yellow and White. They can be accompanied by Dark or Light (except White and Black)
"
" The available groups are:
"       ColorColumn     used for the columns set with 'colorcolumn'
"       Conceal		placeholder characters substituted for concealed text
"       Cursor		the character under the cursor
"       CursorColumn	the screen column that the cursor is in when 'cursorcolumn' is set
"       CursorLine	the screen line that the cursor is in when 'cursorline' is set
"       Directory	directory names
"       DiffAdd		diff mode: Added line
"       DiffChange	diff mode: Changed line
"       DiffDelete	diff mode: Deleted line
"       DiffText	diff mode: Changed text within a changed line
"       ErrorMsg	error messages on the command line
"       VertSplit	the column separating vertically split windows
"       Folded		line used for closed folds
"       FoldColumn	'foldcolumn'
"       SignColumn	column where |signs| are displayed
"       IncSearch	'incsearch' highlighting; also used for the text replaced with
"       LineNr		Line number for ":number" and ":#" commands, and when 'number' or 'relativenumber' option is set.
"       MatchParen	The character under the cursor or just before it, if it is a paired bracket, and its match. |pi_paren.txt|
"       ModeMsg		'showmode' message (e.g., "-- INSERT --")
"       MoreMsg		|more-prompt|
"       NonText		'~' and '@' at the end of the window, characters from
"       		'showbreak' and other characters that do not really exist in the text (e.g., ">" displayed when a double-wide character doesn't fit at the end of the line).
"       Normal		normal text
"       Pmenu		Popup menu: normal item.
"       PmenuSel	Popup menu: selected item.
"       PmenuSbar	Popup menu: scrollbar.
"       PmenuThumb	Popup menu: Thumb of the scrollbar.
"       Question	|hit-enter| prompt and yes/no questions
"       Search		Last search pattern highlighting (see 'hlsearch').
"       		Also used for highlighting the current line in the quickfix window and similar items that need to stand out.
"       SpecialKey	Meta and special keys listed with ":map", also for text used to show unprintable characters in the text, 'listchars'.
"       		really is.
"       SpellBad	Word that is not recognized by the spellchecker.
"       SpellCap	Word that should start with a capital.
"       SpellLocal	Word that is recognized by the spellchecker as one that is used in another region.
"       SpellRare	Word that is recognized by the spellchecker as one that is hardly ever used.
"       StatusLine	status line of current window
"       StatusLineNC	status lines of not-current windows
"       TabLine		tab pages line, not active tab page label
"       TabLineFill	tab pages line, where there are no labels
"       TabLineSel	tab pages line, active tab page label
"       Title		titles for output from ":set all", ":autocmd" etc.
"       Visual		Visual mode selection
"       WarningMsg	warning messages
"       WildMenu	current match in 'wildmenu' completion
"
set background=dark
hi clear
if exists("syntax_on")
  syntax reset
endif

let colors_name = "ruben"

hi Normal       cterm=NONE ctermfg=White ctermbg=Black

"hi ColorColumn 
"hi Conceal
"hi Cursor ctermbg=Red 
"hi CursorColumn
hi CursorLine   cterm=bold ctermfg=NONE ctermbg=DarkGray
hi CursorLineNr cterm=bold ctermfg=LightYellow ctermbg=DarkGray
"hi Directory
"hi DiffAdd
"hi DiffChange
"hi DiffDelete
"hi DiffText
"hi ErrorMsg
"hi VertSplit
"hi Folded
"hi FoldColumn
"hi SignColumn
"hi IncSearch
hi LineNr       cterm=NONE ctermfg=Gray ctermbg=NONE
"hi MatchParen
"hi ModeMsg
"hi MoreMsg
"hi NonText
"hi Pmenu
"hi PmenuSel
"hi PmenuSbar
"hi PmenuThumb
"hi Question
"hi Search
"hi SpecialKey
"hi SpellBad
"hi SpellLocal
"hi SpellRare
"hi StatusLine
"hi StatusLineNC
"hi TabLine
"hi TabLineFill
"hi TabLineSel
"hi Title
"hi Visual
"hi WarningMsg
"hi WildMenu
hi Number       cterm=NONE ctermfg=Red ctermbg=NONE
hi Function     cterm=NONE ctermfg=LightGreen ctermbg=NONE
hi Comment      cterm=NONE ctermfg=Cyan ctermbg=NONE
hi Conditional  cterm=NONE ctermfg=Brown ctermbg=NONE
hi Constant     cterm=NONE ctermfg=Magenta ctermbg=NONE



"function! IsHexColorLight(color) abort
"  let l:raw_color = trim(a:color, '#')
"
"  let l:red = str2nr(substitute(l:raw_color, '(.{2}).{4}', '1', 'g'), 16)
"  let l:green = str2nr(substitute(l:raw_color, '.{2}(.{2}).{2}', '1', 'g'), 16)
"  let l:blue = str2nr(substitute(l:raw_color, '.{4}(.{2})', '1', 'g'), 16)
"
"  let l:brightness = ((l:red * 299) + (l:green * 587) + (l:blue * 114)) / 1000
"
"  return l:brightness > 155
"endfunction
