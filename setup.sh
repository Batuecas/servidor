#Install mysql and create the database ant its tables
apt-get install mysql-server
mysql -u root -e "CREATE USER 'servidor'@'%' IDENTIFIED by '1111';"
mysql -u root -e "CREATE DATABASE servidor;"
mysql -u root -e "GRANT ALL PRIVILEGES ON servidor.* TO 'servidor'@'%';"
mysql -u root -e "CREATE TABLE servidor.formulario (name VARCHAR(20), surname VARCHAR(20), IP VARCHAR(15), time datetime);"
mysql -u root -e "CREATE TABLE servidor.peticion (file VARCHAR(20), language VARCHAR(2), agent VARCHAR(20), IP VARCHAR(15), time datetime, code INT);"
mysql -u root -e "CREATE TABLE servidor.usuarios (username CHAR(50), password CHAR(64) NOT NULL, PRIMARY KEY email VARCHAR(80) NOT NULL);"
mysql -u root -e "CREATE TABLE servidor.descarga (file VARCHAR(50), url VARCHAR(200), IP VARCHAR(16), time DATETIME);"

#Install python3 packages
pip install mysql-connector-python pytube

#Move all files to /root/Servidor and create 'servidor' command
cp -r ./servidor /usr/bin/
cd ..
mv servidor/ /root/
