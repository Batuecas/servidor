import sys, socket, select, gzip, threading, time, datetime, os, mysql.connector, ssl, hashlib, subprocess
from pytube import YouTube 


#función para obtener los parámetros deseados de la cabecera de la petición
def parametros(request):
    try:
        lista_peticion = str(request).split(' ')
        #extreamos el método de la petición
        metodo = lista_peticion[0]
        #si el método es POST, extraemos el referente
        if metodo == 'POST':
            for i in range(0, len(lista_peticion)):
                if lista_peticion[i].endswith('Referer:'):
                    aux = i+1
            referer = lista_peticion[aux].split('\r\n')[0]
        else:
            referer=''
        #extraemos el archivo solicitado
        archivo = lista_peticion[1].split('?')[0].lstrip('/')
        #extraemos el archivo solicitado
        version = lista_peticion[2].split('\r')[0]
        #extraemos el idioma en el que se pide la página
        if archivo=='':
            archivo='index.html'
        archivo=archivo.replace('+', ' ').replace('%20', ' ')
        idioma = request[request.find('Accept-Language')+17: request.find('Accept-Language')+19]
        for i in range(0, len(lista_peticion)):
            if lista_peticion[i].endswith('User-Agent:'):
                aux = i+1
        agent = lista_peticion[aux]
        return metodo, archivo, version, idioma, agent, referer
    except:
        return '', '', '', '', '', ''


#cogemos el version de archivo deseado para especificarlo en la cabecera
def ruta(archivo, idioma, version):
    if(archivo.endswith('.ico')):
        tipo = 'image/ico'
        ruta = 'favicon.ico'
        header, response, codigo=enviar(ruta, version, tipo)
    elif(archivo.endswith('.jpg')):
        tipo = 'image/jpg'
        ruta = 'images/' + archivo
        header, response, codigo=enviar(ruta, version, tipo)
    elif(archivo.endswith('.webp')):
        tipo = 'video/webp'
        ruta = 'videos/' + archivo
        header, response, codigo=enviar(ruta, version, tipo)
    elif(archivo.endswith('.webm')):
        tipo = 'music/webm'
        ruta = 'music/' + archivo
        header, response, codigo=enviar(ruta, version, tipo)
    elif(archivo.endswith('.mp3')):
        tipo = 'music/mp3'
        ruta = 'music/' + archivo
        header, response, codigo=enviar(ruta, version, tipo)
        subprocess.run(["rm", ruta])
    elif(archivo.endswith('.css')):
        tipo = 'text/css'
        ruta = 'styles/' + archivo
        header, response, codigo=enviar(ruta, version, tipo)
    elif(archivo.endswith('.pdf')):
        tipo = 'application/pdf'
        if idioma=='es-ES' or idioma=='es':
            ruta =  'languages/es-ES/' + archivo
        elif idioma=='fr-FR' or idioma=='fr':
            ruta =  'languages/fr-FR/' + archivo
        else:
            ruta =  'languages/es-ES/' + archivo
            idioma = 'es-ES'
        header, response, codigo=enviar(ruta, version, tipo)
    elif(archivo.endswith('.js')):
        tipo = 'text/js'                
        ruta = 'javascript/' + archivo
        header, response, codigo=enviar(ruta, version, tipo)
    elif(archivo.endswith('.json')):
        tipo = 'text/json'                
        ruta = 'data/' + archivo
        header, response, codigo=enviar(ruta, version, tipo)
    elif(archivo.endswith('.xml')):
        tipo = 'text/xml'                
        ruta = 'data/' + archivo
        header, response, codigo=enviar(ruta, version, tipo)
    elif(archivo.endswith('.html')):
        tipo = 'text/html'
        if idioma=='es-ES' or idioma=='es':
            ruta =  'languages/es-ES/' + archivo
        elif idioma=='fr-FR' or idioma=='fr':
            ruta =  'languages/fr-FR/' + archivo
        else:
            ruta =  'languages/es-ES/' + archivo
            idioma = 'es-ES'
        header, response, codigo=enviar(ruta, version, tipo)
    #si el archivo solicitado no termina en .jpg, .css, .pdf o .html la solicitud es errónea
    else:
        header = 'HTTP/1.1 400 Bad Request\n\n'
        response = '<html><body>Error 400: Bad Request</body></html>'.encode('utf-8')
        codigo = 400
    return header, response, codigo


#función para enviar la cabecera y el archivo solicitado
def enviar(ruta, version, tipo) : 
    try:
        file = open(ruta, 'rb')
        response = file.read()
        file.close()
        header = str(version) + ' 200 OK\r\n'
        hora = str(datetime.datetime.now()).split('.')
        header += 'Date: ' + hora[0] + ' GMT\r\n'
        header += 'Connection: keep-alive\r\n'
        header += 'Keep-Alive: timeout=5, max=1000\r\n'
        header += 'Last-Modified: ' + str(datetime.datetime.fromtimestamp(os.path.getmtime(ruta)).strftime("%a, %d %b %Y %H:%M:%S %Z")) +'\r\n'
        header += 'Cache-Control: no-cache, no-store, max-age=0, must-revalidate \r\n'
        header += 'Server: Lab/1.3\r\n'
        header += 'Content-Type: ' + tipo + '; charset=utf-8\r\n'
        header += 'Content-Length: ' + str(os.path.getsize(ruta)) + '\r\n\n'
        codigo = 200
    except:
        header = str(version) + ' 404 Not Found\n\n'
        response='<html><body>Error 404: File not found</body></html>'.encode('utf-8')
        codigo = 404
    return header, response, codigo


#función para comprobar si el usuario y contraseña son correctos
def login(username, password, mycursor):
    pass_hash = hashlib.sha256(password.encode()).hexdigest()
    check_login="SELECT * FROM usuarios WHERE username=%s AND password=%s"
    val=(username, pass_hash)
    mycursor.execute(check_login, val)
    result = mycursor.fetchone()
    if str(type(result)) == "<class 'NoneType'>":
        return False
    else:
        return True


#función para descargar el audio de YouTube
def download_music(name, link):
    try: 
        yt = YouTube(link) 
        yt.title = name
        ruta = './music/'
        video = yt.streams.filter(only_audio=True).order_by('abr').desc().first()
        video.download(ruta)
        subprocess.run(["mv", ruta + name + '.webm', ruta + name + '.mp3'])
    except: 
        print("Connection Error")
    return


#función para tratar los mensajes POST desde formulario.html
def post_form(request, mycursor, formulario, mydb, header, response, codigo):
    try:
        datos=request.split('\r\n\r\n')[1]
        nombre=datos.split('&')[0].split('=')[1]
        apellido=datos.split('&')[1].split('=')[1]
        try:
            val=(nombre, apellido, ip_cliente, hora[0])
            mycursor.execute(formulario, val)
            mydb.commit()
        except:
            header = 'HTTP/1.1 500 Internal Server Error\n\n'
            response = '<html><body>Error 500: Internal Server Error</body></html>'.encode('utf-8')
            codigo = 500
    except:
        header = 'HTTP/1.1 400 Bad Request\n\n'
        response = '<html><body>Error 400: Bad Request</body></html>'.encode('utf-8')
        codigo = 400
    return header, response, codigo


#función para tratar los mensajes POST desde login.html
def post_login(request, mycursor, header, response, codigo):
    try:
        datos=request.split('\r\n\r\n')[1]
        username=datos.split('&')[0].split('=')[1]
        password=datos.split('&')[1].split('=')[1]
    except:
        header = 'HTTP/1.1 400 Bad Request\n\n'
        response = '<html><body>Error 400: Bad Request</body></html>'.encode('utf-8')
        codigo = 400
    signed_in = login(username, password, mycursor)
    if signed_in == False:
        print("ERROR AL INICIAR SESIÓN\n\n")
    return header, response, codigo


#función para tratar los mensajes POST desde descarga.html
def post_down(request, mycursor, descarga, get_down, ip_cliente, hora, mydb, header, response, codigo):
    try:
        datos=request.split('\r\n\r\n')[1]
        name=datos.split('&')[0].split('=')[1]
        url=datos.split('&')[1].split('=')[1]
        url=url.replace('%3A', ':').replace('%2F', '/').replace('%3F', '?').replace('%3D', '=')
        name=name.replace('+', ' ').replace('%20', ' ').replace('%2C', ' ')
        try:
            download_music(name, url)
            val=(name, url, ip_cliente, hora[0])
            mycursor.execute(descarga, val)
            mydb.commit()
            val = [ip_cliente]
            mycursor.execute(get_down, val)
            audio = mycursor.fetchall()[0][0] + '.mp3'
            header = 'HTTP/1.1 301 Temporary Redirect\r\n'
            header += 'Location: ' + audio + '\r\n'
            response = ''.encode('utf-8')
            codigo = 301
        except:
            header = 'HTTP/1.1 500 Internal Server Error\n\n'
            response = '<html><body>Error 500: Internal Server Error</body></html>'.encode('utf-8')
            codigo = 500
    except:
        header = 'HTTP/1.1 400 Bad Request\n\n'
        response = '<html><body>Error 400: Bad Request</body></html>'.encode('utf-8')
        codigo = 400
    return header, response, codigo


#función para tratar los mensajes POST desde datos.htm los mensajes POST desde datos.htmll
def post_datos(request, agent, ip_cliente, hora, mycursor, data, mydb, header, response, codigo):
    try:
        agent=agent.split('\r\n')[0]
        datos=request.split('\r\n\r\n')[1]
        api_key=datos.split('&')[0].split('=')[1]
        sensor=datos.split('&')[1].split('=')[1]
        valor1=datos.split('&')[2].split('=')[1]
        magnitud1=datos.split('&')[3].split('=')[1]
        valor2=datos.split('&')[4].split('=')[1]
        magnitud2=datos.split('&')[5].split('=')[1]
        try:
            val1=(valor1, magnitud1, sensor, api_key, agent, ip_cliente, hora[0])
            mycursor.execute(data, val1)
            mydb.commit()
            val2=(valor2, magnitud2, sensor, api_key, agent, ip_cliente, hora[0])
            mycursor.execute(data, val2)
            mydb.commit()
        except:
            header = 'HTTP/1.1 500 Internal Server Error\n\n'
            response = '<html><body>Error 500: Internal Server Error</body></html>'.encode('utf-8')
            codigo = 500
    except:
        header = 'HTTP/1.1 400 Bad Request\n\n'
        response = '<html><body>Error 400: Bad Request</body></html>'.encode('utf-8')
        codigo = 400

    return header, response, codigo
